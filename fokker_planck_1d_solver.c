#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <gsl/gsl_roots.h>
#include <omp.h>
#include "util.h"

typedef void (*Destroy) (void*);
enum BoundaryCondition {R = 1, A = 0};
typedef enum BoundaryCondition BoundaryCondition;

typedef void (*ImposeBoundary) (double* f, void*);

typedef struct FokkerPlanck1D
{
  double a, b; //boundary points
  double t0; 
  int bc[2];
  int Nx; //number of spatial discretization
  long long int Nt;  //time steps
  double dx, dt;
  double *f, *f_old;
  double survial_old, survial_curr; //survial particles at previous and current time step
  ImposeBoundary left_boundary, right_boundary;

  gsl_function initial;
  gsl_function_fdf potential;
  gsl_function_fdf diffusion;

  long long int save_period;
  char filename[1024];
  FILE* output;

  Destroy destroy_initial;
  Destroy destroy_potential;
  Destroy destroy_diffusion;

} FokkerPlanck1D;

typedef struct TabulatedFunction
{
  int N;
  double dx;
  double* x, *y;
} TabulatedFunction;


typedef struct ConstantFunction
{
  double y;
} ConstantFunction; 


static double constant_f (double x, void* _param)
{
  ConstantFunction* param = (ConstantFunction*)_param;
  return (param->y);
}

static double constant_df (double x, void* _param)
{
  ConstantFunction* param = (ConstantFunction*)_param;
  return 0;
  //return constant_f (x, _param);
}

static void constant_fdf (double x, void* _param, double* f, double* df)
{
  ConstantFunction* param = (ConstantFunction*)_param;
  *f = (param->y);
  *df = 0.;
}


static double tabulated_f (double x, void* _param)
{
  TabulatedFunction* param = (TabulatedFunction*)_param;
  double* xs = param->x;
  double* ys = param->y;
  int N = param->N;
  double dx = param->dx;
  int home = floor ((x-xs[0])/dx);

  if (home >= N)
    return xs[N-1];
  else if (home < 0)
    return xs[0];

  double x0 = xs[home];
  double x1 = xs[home+1];
  double y0 = ys[home];
  double y1 = ys[home+1];

  return (y0*(x1-x)+y1*(x-x0))/dx;
}

static double tabulated_df (double x, void* _param)
{
  TabulatedFunction* param = (TabulatedFunction*)_param;
  double* xs = param->x;
  double* ys = param->y;
  int N = param->N;
  double dx = param->dx;
  int home = floor ((x-xs[0])/dx);
  //if (home >= N-1 || home < 0)
    //return 0.;

  double y0 = ys[home-1];
  double y1 = ys[home+1];

  return (y1-y0)/dx*0.5;
}

static void tabulated_fdf (double x, void* _param, double* f, double* df)
{
  TabulatedFunction* param = (TabulatedFunction*)_param;
  double* xs = param->x;
  double* ys = param->y;
  int N = param->N;
  double dx = param->dx;
  int home = floor ((x-xs[0])/dx);
/*
  if (home >= N)
    home = N-1;
  else if (home < 0)
    home = 0;
  */
  double x0 = xs[home];
  double x1 = xs[home+1];
  double y0 = ys[home];
  double y1 = ys[home+1];
  double y2 = ys[home-1];
  *f = (y0*(x1-x)+y1*(x-x0))/dx;
  *df = (y1-y2)/dx*0.5;
  //*df = (y1-y0)/dx;
}

static void destroy_tabulated (void* _ptr)
{
  gsl_function_fdf* ptr = (gsl_function_fdf*)_ptr;
  TabulatedFunction* params = (TabulatedFunction*)(ptr->params);
  free (params->x);
  free (params->y);
}

static void destroy_constant (void* _ptr)
{
  gsl_function_fdf* ptr = (gsl_function_fdf*)_ptr;
  ConstantFunction* params = (ConstantFunction*)(ptr->params);
  free (params);  
}

static void destroy_delta (void* _ptr)
{
  (void)_ptr;
}

static void reflect_left (double* f, void* _ptr)
{
  FokkerPlanck1D* ptr = (FokkerPlanck1D*)_ptr;

  double a = ptr->a;
  double dx = ptr->dx;
  //double x1 = a+dx;
  double force = -(ptr->potential.df (a, ptr->potential.params));
  //f[0] = f[2]-2.*force*f[1]*dx;
  //f[0] = f[1]-force*f[1]*dx;
  //f[0] = f[1]/(1+dx*force);
  //f[0] = (3.*f[1]-1.5*f[2]+0.3333333333333333*f[3])/(force*dx+11./6.);
  f[0] = (4.*f[1]-f[2])/(2.*force*dx+3.);
}

static void reflect_right (double* f, void* _ptr)
{
  FokkerPlanck1D* ptr = (FokkerPlanck1D*)_ptr;
  double b = ptr->b;
  double dx = ptr->dx;
  //double x1 = b-dx;
  int N = ptr->Nx;
  double force = -(ptr->potential.df (b, ptr->potential.params));
  //f[N] = 2.*force*dx*f[N-1]+f[N-2];
  //f[N] = force*dx*f[N-1]+f[N-1];
  //f[N] = f[N-1]/(1-dx*force);
  f[N] = (4.*f[N-1]-f[N-2])/(3.-2.*force*dx);
}

#if 0
static void reflect_left (double* f, void* _ptr)
{
  FokkerPlanck1D* ptr = (FokkerPlanck1D*)_ptr;

  double a = ptr->a;
  double dx = ptr->dx;
  //double x1 = a+dx;
  double force = -(ptr->potential.df (a, ptr->potential.params));
  //f[0] = f[2]-2.*force*f[1]*dx;
  //f[0] = f[1]-force*f[1]*dx;
  //f[0] = f[1]/(1+dx*force);
  double d0 = ptr->diffusion.f (a,ptr->diffusion.params);
  double d1 = ptr->diffusion.f (a+dx,ptr->diffusion.params);
  double d2 = ptr->diffusion.f (a+2.*dx,ptr->diffusion.params);
  f[0] = (4.*d1*f[1]-d2*f[2])/(d0*(2.*force*dx+3.));
}

static void reflect_right (double* f, void* _ptr)
{
  FokkerPlanck1D* ptr = (FokkerPlanck1D*)_ptr;
  double b = ptr->b;
  double dx = ptr->dx;
  //double x1 = b-dx;
  int N = ptr->Nx;
  double force = -(ptr->potential.df (b, ptr->potential.params));
  double d0 = ptr->diffusion.f (b,ptr->diffusion.params);
  double d1 = ptr->diffusion.f (b-dx,ptr->diffusion.params);
  double d2 = ptr->diffusion.f (b-2.*dx,ptr->diffusion.params);
  //f[N] = 2.*force*dx*f[N-1]+f[N-2];
  //f[N] = force*dx*f[N-1]+f[N-1];
  //f[N] = f[N-1]/(1-dx*force);
  f[N] = (4.*d1*f[N-1]-d2*f[N-2])/(d0*(3.-2.*force*dx));
}
#endif

static void absorb_right (double* f, void* _ptr)
{
  FokkerPlanck1D* ptr = (FokkerPlanck1D*)_ptr;
  f[ptr->Nx] = 0;
}

static void absorb_left (double* f, void* _ptr)
{
  f[0] = 0;
}

void* AllocFokkerPlanck1D (double a, double b, int N, int* bc)
{
  FokkerPlanck1D* ptr = (FokkerPlanck1D*) malloc (sizeof (FokkerPlanck1D));
  memset (ptr, 0, sizeof (FokkerPlanck1D));
  if (b <= a)
    exit (-1);
  ptr->a = a;
  ptr->b = b;
  
  ptr->bc[0] = bc[0];
  ptr->bc[1] = bc[1];

  if (ptr->bc[0] == R)
    ptr->left_boundary = &reflect_left;
  else if (ptr->bc[0] == A)
    ptr->left_boundary = &absorb_left;
  else
    exit (-1);

  if (ptr->bc[1] == R)
    ptr->right_boundary = &reflect_right;
  else if (ptr->bc[1] == A)
    ptr->right_boundary = &absorb_right;
  else
    exit (-1);

  if (N < 2)
    exit (-1);
  ptr->Nx = N;
  ptr->dx = (b-a)/N;
  ptr->f = (double*) malloc (sizeof (double)*(N+1));
  memset (ptr->f ,0, sizeof (double)*(N+1));

  ptr->f_old = (double*) malloc (sizeof (double)*(N+1));
  memset (ptr->f_old, 0, sizeof (double)*(N+1));

  return ptr; 
}

void DestroyFokkerPlanck1D (void* _ptr)
{
  FokkerPlanck1D* ptr = (FokkerPlanck1D*) _ptr;
  free (ptr->f);
  free (ptr->f_old);
  ptr->destroy_initial (&(ptr->initial));
  ptr->destroy_potential (&(ptr->potential));
  ptr->destroy_diffusion (&(ptr->diffusion));
  fclose (ptr->output);
  free (ptr);
}

static void read_table (TabulatedFunction* tab, char* filename)
{
  FILE* fpr = fopen (filename, "rt");
  if (fpr == NULL)
    {
      fprintf (stderr, "error %s %d\n", __FILE__, __LINE__); 
      exit(-1);
    }
  char** argv = NULL;
  int argc = 0;
  char* line = NULL;
  size_t nbytes = 0;
  getline(&line, &nbytes, fpr);
  trim_both_sides(line);
  int num = atoi (line);
  tab->N = num;
  tab->x = malloc (sizeof (double)*num);
  tab->y = malloc (sizeof (double)*num);
  int count = 0;
  while(getline(&line, &nbytes, fpr)>=0)
    {
        trim_both_sides(line);
        if(strlen(line) == 0)
            continue;
        extract_arguments(&argv, &argc, line);
        if (argc != 2)
        {
          fprintf (stderr, "error %s %d\n", __FILE__, __LINE__);
          exit(-1);
        }
        double x = (double)atof (argv[0]);
        double y = (double)atof (argv[1]);
        tab->x[count] = x;
        tab->y[count] = y;
        count++;
        if (count > num)
        {
          fprintf (stderr, "error %s %d\n", __FILE__, __LINE__);
          exit (-1);
        }
    }
  fclose (fpr);
  free(line);
  for(int i = 0; i < argc; ++i)
      free(argv[i]);
  free(argv);
  tab->dx = tab->x[1]-tab->x[0];
}

void SetPotential (void* _ptr, char** argv, int argc)
{
  char* type = argv[0];
  FokkerPlanck1D* ptr = (FokkerPlanck1D*)_ptr;
 
  if (!strcmp (type, "tabulated"))
    {
      ptr->potential.f = &tabulated_f;
      ptr->potential.df = &tabulated_df;
      ptr->potential.fdf = &tabulated_fdf;
 
      char* filename = argv[1];
      TabulatedFunction* tab = malloc (sizeof (TabulatedFunction));
      read_table (tab, filename); 
      ptr->potential.params = tab;
      ptr->destroy_potential = &destroy_tabulated;
    }
  else if (!strcmp (type, "constant"))
    {
      if (argc != 2)
        exit (-1);
      ConstantFunction* param = malloc (sizeof (ConstantFunction));
      param->y = atof (argv[1]);
      ptr->potential.f = &constant_f;
      ptr->potential.df = &constant_df;
      ptr->potential.fdf = &constant_fdf;
      ptr->potential.params = param;
      ptr->destroy_potential = &destroy_constant;
    }
  else
    {
      fprintf (stderr, "not supported\n");
      exit (-1);
    }
}

static void set_delta_function (FokkerPlanck1D* ptr, double pos)
{
  double a = ptr->a;
  double dx = ptr->dx;

  int home = (pos-a)/dx;
  ptr->f[home] = 1./dx;
  ptr->f_old[home] = 1./dx;
}

void SetInitialCondition (void* _ptr, char** argv, int argc)
{
  char* type = argv[0];
  FokkerPlanck1D* ptr = (FokkerPlanck1D*)_ptr;

  if (!strcmp (type, "delta"))
    {
      double pos = (double)atof (argv[1]);
      set_delta_function (ptr, pos);
      ptr->destroy_initial = &destroy_delta;
    }

  ptr->left_boundary (ptr->f,ptr);
  ptr->right_boundary (ptr->f,ptr);
  ptr->left_boundary (ptr->f_old,ptr);
  ptr->right_boundary (ptr->f_old,ptr);
  
}

void SetDiffusion (void* _ptr, char** argv, int argc)
{
  char* type = argv[0];
  FokkerPlanck1D* ptr = (FokkerPlanck1D*)_ptr;
  if (!strcmp (type, "tabulated"))
    {
      ptr->diffusion.f = &tabulated_f;
      ptr->diffusion.df = &tabulated_df;
      ptr->diffusion.fdf = &tabulated_fdf;
 
      char* filename = argv[1];
      TabulatedFunction* tab = malloc (sizeof (TabulatedFunction));
      read_table (tab, filename); 
      ptr->diffusion.params = tab;
      ptr->destroy_diffusion = &destroy_tabulated;
    }
  else if (!strcmp (type, "constant"))
    {
      if (argc != 2)
        exit (-1);
      ConstantFunction* param = malloc (sizeof (ConstantFunction));
      param->y = atof (argv[1]);
      ptr->diffusion.f = &constant_f;
      ptr->diffusion.df = &constant_df;
      ptr->diffusion.fdf = &constant_fdf;
      ptr->diffusion.params = param;
      ptr->destroy_diffusion = &destroy_constant;
    }
}

static double integrate (FokkerPlanck1D* ptr)
{
  int N = ptr->Nx;
  double sum = 0.;
  for (int i = 0; i < N; ++i)
      sum += 0.5*(ptr->f[i]+ptr->f[i+1])*ptr->dx;
  return sum; 
}

static void one_step (FokkerPlanck1D* ptr, double dt)
{
  double* f = ptr->f;
  double* f_old = ptr->f_old;
  int N = ptr->Nx;
  double dx = ptr->dx;
  double inv_dx = 1./dx;
  double a = ptr->a;

  omp_set_num_threads(8);
  #pragma omp parallel for
  for (int i = 1; i < N; ++i)
    {
      double x_i = a+i*dx;
      double x_ip1 = a+(i+1)*dx;
      double x_im1 = a+(i-1)*dx;

      double D = ptr->diffusion.f (x_i, ptr->diffusion.params);
      double dDdx = ptr->diffusion.df (x_i, ptr->diffusion.params);
      //double D_ip1 = ptr->diffusion.f (x_ip1, ptr->diffusion.params);
      //double D_im1 = ptr->diffusion.f (x_im1, ptr->diffusion.params);

      double F_i =  -(ptr->potential.df (x_i, ptr->potential.params));
      double F_ip1 = -(ptr->potential.df (x_ip1, ptr->potential.params));
      double F_im1 = -(ptr->potential.df (x_im1, ptr->potential.params));

      double p_i = f_old[i];
      double p_ip1 = f_old[i+1];
      double p_im1 = f_old[i-1];

      double incr = dDdx*(p_ip1-p_im1)*0.5*inv_dx + //p_i*(D_ip1+D_im1-2.*D)*inv_dx*inv_dx+
                    D*(p_ip1+p_im1-2.*p_i)*inv_dx*inv_dx - 
                    //(D_ip1*p_ip1+D_im1*p_im1-2.*p_i*D)*inv_dx*inv_dx -
                    //dDdx*F_i*p_i-D*(F_ip1*p_ip1-F_im1*p_im1)*0.5*inv_dx;
                    dDdx*F_i*p_i-0.5*inv_dx*(D*F_i*(p_ip1-p_im1)+D*p_i*(F_ip1-F_im1));
      f[i] = p_i+dt*incr;
    }
  ptr->left_boundary (f,ptr);
  ptr->right_boundary (f,ptr);

  double* tmp = ptr->f_old;
  ptr->f_old = ptr->f;
  ptr->f = tmp;
}

static void write_header (FokkerPlanck1D* ptr)
{
  ptr->output = fopen (ptr->filename, "wb");
  if (ptr->output == NULL)
    exit (-1);

  int32_t out = 0;
  fwrite (&out, sizeof (int32_t), 1, ptr->output);
  fflush (ptr->output);

  out = ptr->Nx+1;
  fwrite (&out, sizeof (int32_t), 1, ptr->output);
  fflush (ptr->output);

  out = ptr->save_period;
  fwrite (&out, sizeof (int32_t), 1, ptr->output);
  fflush (ptr->output); 

  fwrite (&(ptr->dt), sizeof (double), 1, ptr->output); 
  fflush (ptr->output);
}

static void write_output (FokkerPlanck1D* ptr, int count)
{
  fseek (ptr->output, 0, SEEK_SET);
  int32_t out = count;
  fwrite (&out, sizeof (int32_t), 1, ptr->output);
  fseek (ptr->output, 0, SEEK_END);
  fwrite (ptr->f_old, sizeof (double), ptr->Nx+1, ptr->output); 
  //fflush (ptr->output);
  fflush (ptr->output);
}

void read_restart (void* _ptr, char* filename, double t0)
{
  FokkerPlanck1D *ptr = (FokkerPlanck1D*) _ptr;
  FILE* fp = fopen (filename, "rb");
  fread (ptr->f_old, sizeof (double), ptr->Nx+1, fp);
  memcpy (ptr->f, ptr->f_old, sizeof (double)*(ptr->Nx+1));
  fclose (fp);
  ptr->t0 = t0;
}

static void save_restart (FokkerPlanck1D *ptr)
{
  FILE* fp = fopen ("restart.bin", "wb");
  fwrite (ptr->f_old, sizeof (double), ptr->Nx+1, fp); 
  fflush (fp);
  fclose (fp);
}

void Run (void* _ptr, double dt, long long int Nt, long long int save_period, char* filename)
{
  FokkerPlanck1D* ptr = (FokkerPlanck1D*)_ptr;
  ptr->dt = dt;
  ptr->Nt = Nt;
  ptr->survial_curr = integrate (ptr);

  ptr->save_period = save_period;
  strcpy (ptr->filename, filename);
  int count = 0;
  write_header (ptr);
  write_output (ptr, 1);
  count++;
  for (long long int t = 1ll; t <= Nt; ++t)
    {       
      one_step (ptr, 0.5*dt);
      one_step (ptr, 0.5*dt);
      //write out step if needed
      if (t % save_period == 0)
        {
          count++;
          write_output (ptr, count);
          //and compute the fpt distribution
          ptr->survial_old = ptr->survial_curr;
          ptr->survial_curr = integrate (ptr);
          double fptd = (ptr->survial_old-ptr->survial_curr)*1000000./(dt*save_period);
          printf ("%.12lf %.12lf\n", 0.5*((2.*t-(double)save_period)*dt/1000000.)+ptr->t0, fptd);
          fflush(stdout);
        }
    } 
  save_restart (ptr);
}
