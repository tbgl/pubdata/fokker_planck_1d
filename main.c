#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "util.h"
#include "fokker_planck_1d_solver.h"

int main (int argc, char** argv)
{
  double a, b;
  int N;
  int bc[2];
  int pos;
  pos = extract_options (argv, argc, "-boundary", 5);
  if (pos < 0)
    {
      fprintf (stderr, "not found options %s\n", "-boundary");
      exit (-1);
    }
  a = (double) atof (argv[pos+1]);
  b = (double) atof (argv[pos+2]); 
  N = atoi (argv[pos+3]);
  bc[0] = atoi (argv[pos+4]);
  bc[1] = atoi (argv[pos+5]);

  void* fokker_planck1d = AllocFokkerPlanck1D (a,b, N, bc);
  
  int num_options;
  num_options = extract_num_options (argv, argc, "-potential");
  if (num_options < 0)
    exit (-1);
  pos = extract_options (argv, argc, "-potential", num_options);
  if (pos < 0)
    {
      fprintf (stderr, "not found options %s\n", "-potential");
      exit (-1);
    }
  SetPotential (fokker_planck1d, argv+pos+1, num_options);

  num_options = extract_num_options (argv, argc, "-diffusion");
  if (num_options < 0)
    exit (-1);
  pos = extract_options (argv, argc, "-diffusion", num_options);
  if (pos < 0)
    {
      fprintf (stderr, "not found options %s\n", "-diffusion");
      exit (-1);
    }  
  SetDiffusion (fokker_planck1d, argv+pos+1, num_options); 

  pos = extract_options (argv, argc, "-initial", 2);
  if (pos < 0)
    {
      fprintf (stderr, "not found options %s\n", "-initial");
      exit (-1);
    }
  SetInitialCondition (fokker_planck1d, argv+pos+1, 2);

  pos = extract_options (argv, argc, "-restart", 1);
  if (pos >= 0)
    {
      read_restart (fokker_planck1d, argv[pos+1], (double) atof(argv[pos+2]));
    }

  double dt;
  long long int Nt;
  long long int save_period;
  char filename[1024];
  pos = extract_options (argv, argc, "-run", 2);
  if (pos < 0)
    {
      fprintf (stderr, "not found options %s\n", "-run");
      exit (-1);
    }
  dt = (double) atof (argv[pos+1]);
  Nt = atoll (argv[pos+2]);
  save_period = atoll (argv[pos+3]);
  //printf ("%lld %lld\n", Nt, save_period);
  strcpy (filename, argv[pos+4]);

  Run (fokker_planck1d, dt, Nt, save_period, filename);
  DestroyFokkerPlanck1D (fokker_planck1d);
  return 0;
}
