import sys

fpt = []
time = []
with open(sys.argv[1],"rt") as f:
    lines = f.readlines()
    for line in lines:
        if(len(line)>0):
            words = line.split()
            time.append(float(words[0]))
            fpt.append(float(words[1]))

count = 0.
mean = 0.
dt = time[1]-time[0]
for i in range (len(fpt)):
    mean += time[i]*fpt[i]*dt
    count += fpt[i]*dt
print("MFPT "+str(mean/count)+" ns")
