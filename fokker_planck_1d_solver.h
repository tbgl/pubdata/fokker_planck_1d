#ifndef FOKKERPLANCK1D_H_
#define FOKKERPLANCK1D_H_
void* AllocFokkerPlanck1D (double a, double b, int, int*);
void SetPotential (void* _ptr, char** argv, int argc);
void SetInitialCondition (void* _ptr, char** argv, int argc);
void SetDiffusion (void* _ptr, char** argv, int argc);
void Run (void* _ptr, double dt, long long int Nt, long long int save_period, char* filename);
void DestroyFokkerPlanck1D (void* _ptr);
void read_restart (void* _ptr, char* filename, double);
#endif
