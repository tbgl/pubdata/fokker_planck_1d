CC:=gcc

ifeq ($(debug), 1)
	CFLAGS:=-std=c99 -pedantic -ggdb -Ddebug -D_GNU_SOURCE
else
	CFLAGS:=-std=c99 -O3 -pedantic -Wall -Wextra -D_GNU_SOURCE
endif

ifeq ($(debug), 1)
	EXE:=./bin/fpsolver_dbg
else
	EXE:=./bin/fpsolver
endif

TUTORIAL:=tutorial.pdf

code: ${EXE}
tutorial: ${TUTORIAL}

all:${EXE} ${TUTORIAL}

main.o: main.c ./util/util.h ./util/util.c 
	${CC} ${CFLAGS} -c main.c -I./util/ -o main.o

fokker_planck_1d_solver.o: fokker_planck_1d_solver.c fokker_planck_1d_solver.h
	${CC} ${CFLAGS} -fopenmp -lgsl -c fokker_planck_1d_solver.c -I./util -o fokker_planck_1d_solver.o

./util/util.o: ./util/util.c ./util/util.h
	${CC} ${CFLAGS} -c util/util.c -I./util -o ./util/util.o

${EXE}: main.o fokker_planck_1d_solver.o ./util/util.o
	${CC} ${CFLAGS} -fopenmp -lgsl main.o fokker_planck_1d_solver.o ./util/util.o -o ${EXE} -lm

${TUTORIAL}: tutorial.tex
	pdflatex tutorial.tex

clean:
	rm -r -f *.o ./util/*.o ${EXE}


